package com.uliana.squatchampion.data.database.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RealmSquatCategory extends RealmObject {

    @PrimaryKey
    private String title;
}
