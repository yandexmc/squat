package com.uliana.squatchampion.data.database;


import android.content.Context;

import com.uliana.squatchampion.data.database.model.RealmSquatCategory;
import com.uliana.squatchampion.data.database.model.RealmSquatResult;
import com.uliana.squatchampion.domain.converter.database.SquatCategoryConverter;
import com.uliana.squatchampion.domain.converter.database.SquatResultConverter;
import com.uliana.squatchampion.domain.model.dto.SquatCategoryDto;
import com.uliana.squatchampion.domain.model.dto.SquatResultDto;
import com.uliana.squatchampion.util.DateUtils;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class RealmApi extends AbstractRealmApi {

    private static final String REALM_NAME = "realm_api";

    private final SquatCategoryConverter squatCategoryConverter = new SquatCategoryConverter();
    private final SquatResultConverter squatResultConverter = new SquatResultConverter();

    @Inject
    public RealmApi(Context context) {
        final RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context)
                .name(REALM_NAME)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    public void saveCategory(RealmSquatCategory category) {
        realmInstance.executeTransaction(realmTr -> {
            realmTr.copyToRealm(category);
        });
    }

    public List<SquatCategoryDto> loadCategories() {
        final RealmResults<RealmSquatCategory> result = realmInstance.where(RealmSquatCategory.class).findAll();
        final List<RealmSquatCategory> resultList = realmInstance.copyFromRealm(result);

        return squatCategoryConverter.fromBusiness(resultList);
    }

    public void saveResult(RealmSquatResult realmSquatResult) {
        realmInstance.executeTransaction(realmTr -> {
            realmTr.copyToRealmOrUpdate(realmSquatResult);
        });
    }

    public List<SquatResultDto> loadResults() {
        final RealmResults<RealmSquatResult> result = realmInstance.where(RealmSquatResult.class).findAll();
        final List<RealmSquatResult> resultList = realmInstance.copyFromRealm(result);

        return squatResultConverter.fromBusiness(resultList);
    }

    public SquatResultDto getTodayResult() {
        final RealmResults<RealmSquatResult> result = realmInstance.where(RealmSquatResult.class).findAll();
        final List<RealmSquatResult> resultList = realmInstance.copyFromRealm(result);

        return CollectionUtils.find(squatResultConverter.fromBusiness(resultList),
                object -> object.getDate().equals(DateUtils.toDate(DateUtils.nowMillis())));
    }

}
