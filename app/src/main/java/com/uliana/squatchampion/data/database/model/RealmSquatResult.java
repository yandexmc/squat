package com.uliana.squatchampion.data.database.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RealmSquatResult extends RealmObject {

    @PrimaryKey
    private long date;
    private RealmSquatCategory category;
    private int count;
}
