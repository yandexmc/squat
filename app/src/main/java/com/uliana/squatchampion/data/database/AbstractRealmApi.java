package com.uliana.squatchampion.data.database;

import io.realm.Realm;

public abstract class AbstractRealmApi {


    protected Realm realmInstance;

    public void open() {
        if (realmInstance == null) {
            realmInstance = Realm.getDefaultInstance();
        }
    }

    public void close() {
        if (realmInstance != null) {
            realmInstance.close();
        }
        realmInstance = null;
    }

}
