package com.uliana.squatchampion.data.shared_preferences;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.functions.Action1;

@Singleton
public class Preferences {

    private Context context;

    @Inject
    public Preferences(Context context) {
        this.context = context;
    }

    private static final String SELECTED_CATEGORY = "SELECTED_CATEGORY";


    public String readSelectedCategory() {
        return getPreferences().getString(SELECTED_CATEGORY, "");
    }

    public void writeSelectedCategory(final String categoryTitle) {
        editPreferences(editor -> editor.putString(SELECTED_CATEGORY, categoryTitle));
    }

    private SharedPreferences getPreferences() {
        return context.getSharedPreferences(context.getApplicationContext().getPackageName(), Context.MODE_PRIVATE);
    }

    private void editPreferences(final Action1<SharedPreferences.Editor> action) {
        final SharedPreferences.Editor editor = getPreferences().edit();
        action.call(editor);
        editor.apply();
    }
}
