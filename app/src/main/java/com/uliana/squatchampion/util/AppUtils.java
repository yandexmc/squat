package com.uliana.squatchampion.util;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uliana.squatchampion.presentation.common.inflater.Layout;

@SuppressWarnings({"WeakerAccess", "SameParameterValue"})
public final class AppUtils {

    private AppUtils() {
        throw new UnsupportedOperationException("can't create instance of AppUtils class");
    }

    public static Intent withExtras(final Intent intent, final Bundle extras) {
        if (extras != null) {
            intent.putExtras(extras);
        }
        return intent;
    }

    public static View inflateLayout(final LayoutInflater inflater, final ViewGroup parent, final Object source) {
        final Class sourceClass = source.getClass();

        if (sourceClass.isAnnotationPresent(Layout.class)) {
            final Layout annotation = (Layout) sourceClass.getAnnotation(Layout.class);
            return inflater.inflate(annotation.id(), parent, false);
        }

        return null;
    }

    @LayoutRes
    public static int getLayoutId(@NonNull final Object source) {
        final Class clazz = source.getClass();
        if (!clazz.isAnnotationPresent(Layout.class)) {
            return 0;
        }
        final Layout layoutAnnotation = (Layout) clazz.getAnnotation(Layout.class);
        return layoutAnnotation.id();
    }

    public static void changeViewVisibility(final boolean visibility,
                                            final View... views) {
        changeViewVisibility(visibility ? View.VISIBLE : View.GONE, views);
    }

    public static void changeViewVisibility(final int visibility,
                                            final View... views) {
        if (views != null && views.length > 0) {
            for (final View view : views) {
                if (view != null) {
                    view.setVisibility(visibility);
                }
            }
        }
    }

}
