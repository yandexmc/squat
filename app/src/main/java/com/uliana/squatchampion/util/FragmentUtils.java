package com.uliana.squatchampion.util;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.uliana.squatchampion.R;
import com.uliana.squatchampion.presentation.common.state.AnimationType;

import java.util.List;

@SuppressWarnings({"WeakerAccess", "SameParameterValue", "unused"})
public final class FragmentUtils {

    private FragmentUtils() {
        throw new UnsupportedOperationException("can't create instance of FragmentUtils class");
    }

    public static <T extends Fragment> T withArgs(final T fragment, final Bundle args) {
        if (args != null) {
            fragment.setArguments(args);
        }

        return fragment;
    }

    public static void removeFragment(final FragmentManager fragmentManager, final Fragment fragment) {
        if (fragmentManager != null) {
            final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if (fragment != null) {
                fragmentTransaction.remove(fragment);
            }

            fragmentTransaction.commit();
        }
    }

    public static void clearBackStack(final FragmentManager fragmentManager) {
        if (fragmentManager == null) {
            return;
        }

        while (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStackImmediate();
        }

        final List<Fragment> fragments = fragmentManager.getFragments();
        for (int i = fragments.size() - 1; i > 0; i--) {
            FragmentUtils.removeFragment(fragmentManager, fragments.get(i));
        }

        while (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStackImmediate();
        }
    }

    public static void replaceFragment(final FragmentManager fragmentManager,
                                       final Fragment fragment,
                                       final AnimationType animationType) {
        FragmentUtils.startFragment(fragmentManager,
                fragment,
                R.id.fragment_container,
                true,
                true,
                null,
                animationType);
    }

    public static void startFragment(final FragmentManager fragmentManager,
                                     final Fragment fragment,
                                     @IdRes final int containerId,
                                     final boolean needAddToBackStack,
                                     final boolean needToReplace,
                                     final Fragment removingFragment,
                                     final AnimationType animation) {
        if (fragmentManager == null) {
            return;
        }

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (animation) {
            case FROM_BOTTOM:
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_top, R.anim.enter_from_top, R.anim.exit_to_bottom);
                break;
            case FADE:
                fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
                break;
            case FROM_RIGHT:
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                break;
            case NO_ANIMATION:
                break;
        }

        final String fragmentName = fragment.getClass().getSimpleName();
        if (needToReplace) {
            fragmentTransaction.replace(containerId, fragment, fragmentName);
        } else {
            fragmentTransaction.add(containerId, fragment, fragmentName);
        }

        if (needAddToBackStack) {
            fragmentTransaction.addToBackStack(fragmentName);
        }

        if (removingFragment != null) {
            fragmentTransaction.remove(removingFragment);
        }

        fragmentTransaction.commit();
    }

}
