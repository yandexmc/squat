package com.uliana.squatchampion.util;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public final class DateUtils {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("dd.MM.yyyy");

    private DateUtils() {
    }

    public static String today() {
        return toDate(nowMillis());
    }

    public static long nowMillis() {
        return org.joda.time.DateTimeUtils.currentTimeMillis();
    }

    public static String toDate(final long timestamp) {
        return DATE_FORMATTER.print(timestamp);
    }

}
