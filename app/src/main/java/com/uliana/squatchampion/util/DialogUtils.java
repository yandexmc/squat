package com.uliana.squatchampion.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.EditText;

import com.uliana.squatchampion.R;


@SuppressWarnings({"WeakerAccess", "SameParameterValue"})
public final class DialogUtils {

    public static final DialogInterface.OnClickListener DISMISS_LISTENER = (dialogInterface, which) -> dialogInterface.dismiss();

    private DialogUtils() {
        throw new UnsupportedOperationException("can't create instance of DialogUtils class");
    }

    public static AlertDialog createEditTextDialog(@NonNull final Context context,
                                                   final String title,
                                                   final boolean cancelable,
                                                   final EnteredDialogTextListener listener) {

        final LayoutInflater inflater = LayoutInflater.from(context);
        EditText editText = (EditText) inflater.inflate(R.layout.include_edit_text, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setView(editText)
                .setCancelable(cancelable)
                .setNeutralButton(R.string.dialog_neutral, DISMISS_LISTENER)
                .setPositiveButton(R.string.dialog_positive,
                        (dialogInterface, which) -> {
                            String text = String.valueOf(editText.getText());
                            if (!text.isEmpty())
                                listener.onTextEntered(text);
                            dialogInterface.dismiss();
                        });


        return builder.create();

    }

    public interface EnteredDialogTextListener {
        void onTextEntered(String text);
    }

}
