package com.uliana.squatchampion.domain.converter.database;

import android.support.annotation.NonNull;

import com.uliana.squatchampion.data.database.model.RealmSquatResult;
import com.uliana.squatchampion.domain.converter.CollectionConverter;
import com.uliana.squatchampion.domain.model.dto.SquatResultDto;
import com.uliana.squatchampion.util.DateUtils;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SquatResultConverter extends CollectionConverter<List<RealmSquatResult>, List<SquatResultDto>> {

    @Inject
    public SquatResultConverter() {
    }

    @Override
    protected List<SquatResultDto> fromBusinessNullSafe(@NonNull List<RealmSquatResult> business) {
        List<SquatResultDto> resultDtoList = new ArrayList<>();

        SquatResultDto squatResultDto;
        int dayIndex = 0;
        for (RealmSquatResult realmSquatResult : business) {
            final String date = DateUtils.toDate(realmSquatResult.getDate());
            squatResultDto = CollectionUtils.find(resultDtoList, object -> object.getDate().equals(date));
            if (squatResultDto == null) {
                resultDtoList.add(new SquatResultDto(date, realmSquatResult.getCategory().getTitle(),
                        String.valueOf(realmSquatResult.getCount()), realmSquatResult.getCount(), 0, dayIndex));
                dayIndex++;
            } else {
                squatResultDto.setCount(squatResultDto.getCount() + realmSquatResult.getCount());
                squatResultDto.setResults(squatResultDto.getResults() + "+" + realmSquatResult.getCount());
                squatResultDto.setRoundsCount(squatResultDto.getRoundsCount() + 1);
            }
        }
        return resultDtoList;
    }

    @Override
    protected List<RealmSquatResult> fromDtoNullSafe(@NonNull List<SquatResultDto> dto) {
        return null;
    }
}
