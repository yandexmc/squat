package com.uliana.squatchampion.domain.model.dto;


import android.os.Parcel;
import android.os.Parcelable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SquatResultDto implements Parcelable {

    private String date;
    private String category;
    private String results;
    private int count;
    private int roundsCount;
    private int dayNumber;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.date);
        dest.writeString(this.category);
        dest.writeString(this.results);
        dest.writeInt(this.count);
        dest.writeInt(this.roundsCount);
        dest.writeInt(this.dayNumber);
    }

    public SquatResultDto() {
    }

    protected SquatResultDto(Parcel in) {
        this.date = in.readString();
        this.category = in.readString();
        this.results = in.readString();
        this.count = in.readInt();
        this.roundsCount = in.readInt();
        this.dayNumber = in.readInt();
    }

    public static final Creator<SquatResultDto> CREATOR = new Creator<SquatResultDto>() {
        @Override
        public SquatResultDto createFromParcel(Parcel source) {
            return new SquatResultDto(source);
        }

        @Override
        public SquatResultDto[] newArray(int size) {
            return new SquatResultDto[size];
        }
    };
}
