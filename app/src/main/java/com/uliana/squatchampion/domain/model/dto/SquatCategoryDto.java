package com.uliana.squatchampion.domain.model.dto;


import android.os.Parcel;
import android.os.Parcelable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SquatCategoryDto implements Parcelable {

    private String title;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
    }

    public SquatCategoryDto() {
    }

    protected SquatCategoryDto(Parcel in) {
        this.title = in.readString();
    }

    public static final Creator<SquatCategoryDto> CREATOR = new Creator<SquatCategoryDto>() {
        @Override
        public SquatCategoryDto createFromParcel(Parcel source) {
            return new SquatCategoryDto(source);
        }

        @Override
        public SquatCategoryDto[] newArray(int size) {
            return new SquatCategoryDto[size];
        }
    };
}
