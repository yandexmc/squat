package com.uliana.squatchampion.domain.converter.database;

import android.support.annotation.NonNull;

import com.uliana.squatchampion.data.database.model.RealmSquatCategory;
import com.uliana.squatchampion.domain.converter.CollectionConverter;
import com.uliana.squatchampion.domain.model.dto.SquatCategoryDto;

import javax.inject.Inject;

public class SquatCategoryConverter extends CollectionConverter<RealmSquatCategory, SquatCategoryDto> {

    @Inject
    public SquatCategoryConverter() {
    }

    @Override
    protected SquatCategoryDto fromBusinessNullSafe(@NonNull RealmSquatCategory business) {
        final SquatCategoryDto squatCategoryDto = new SquatCategoryDto();
        squatCategoryDto.setTitle(business.getTitle());

        return squatCategoryDto;
    }

    @Override
    protected RealmSquatCategory fromDtoNullSafe(@NonNull SquatCategoryDto dto) {
        throw new UnsupportedOperationException("can't create BackendExampleDto from LocalExampleDto");
    }
}
