package com.uliana.squatchampion.domain.model.event;

import com.uliana.squatchampion.presentation.common.state.AppearanceType;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AppearanceChangedEvent {
    private AppearanceType appearanceType;
}
