package com.uliana.squatchampion.domain.model.dto;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.uliana.squatchampion.R;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MenuItemDto {
    HOME(R.drawable.ic_menu_home, R.string.menu_item_home),
    HISTORY(R.drawable.ic_menu_history, R.string.menu_item_history),
    CATEGORIES(R.drawable.ic_menu_category, R.string.menu_item_categories);

    @DrawableRes
    private final int iconId;
    @StringRes
    private final int stringId;

}
