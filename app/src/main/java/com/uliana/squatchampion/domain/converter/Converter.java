package com.uliana.squatchampion.domain.converter;

import android.support.annotation.Nullable;

public interface Converter<BusinessObject, DataTransferObject> {

    DataTransferObject fromBusiness(@Nullable final BusinessObject business);

    BusinessObject fromDto(@Nullable final DataTransferObject dto);
}
