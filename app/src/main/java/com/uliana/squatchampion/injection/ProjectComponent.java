package com.uliana.squatchampion.injection;


import com.uliana.squatchampion.injection.module.SampleModule;
import com.uliana.squatchampion.presentation.history.HistoryFragment;
import com.uliana.squatchampion.presentation.history.HistoryPresenter;
import com.uliana.squatchampion.presentation.home.HomeFragment;
import com.uliana.squatchampion.presentation.home.HomePresenter;
import com.uliana.squatchampion.presentation.main.AuthActivity;
import com.uliana.squatchampion.presentation.main.MainActivity;
import com.uliana.squatchampion.presentation.main.MainPresenter;
import com.uliana.squatchampion.presentation.category.CategoryListFragment;
import com.uliana.squatchampion.presentation.category.CategoryListPresenter;
import com.uliana.squatchampion.presentation.main.menu.LeftMenuFragment;
import com.uliana.squatchampion.presentation.main.menu.LeftMenuPresenter;
import com.uliana.squatchampion.presentation.main.toolbar.ToolbarHolder;
import com.uliana.squatchampion.presentation.main.toolbar.ToolbarPresenter;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {SampleModule.class})
public interface ProjectComponent {
    Bus bus();

    //ACTIVITY
    void inject(final MainActivity activity);

    void inject(final AuthActivity activity);

    //CUSTOM COMPONENTS
    void inject(final ToolbarHolder holder);

    //FRAGMENTS
    void inject(final CategoryListFragment fragment);

    void inject(final LeftMenuFragment fragment);

    void inject(final HomeFragment fragment);

    void inject(final HistoryFragment fragment);


    //PRESENTERS
    void inject(final LeftMenuPresenter presenter);

    void inject(final ToolbarPresenter presenter);

    void inject(final CategoryListPresenter presenter);

    void inject(final MainPresenter presenter);

    void inject(final HomePresenter presenter);

    void inject(final HistoryPresenter presenter);
}
