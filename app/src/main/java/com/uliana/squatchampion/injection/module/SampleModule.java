package com.uliana.squatchampion.injection.module;

import android.app.Application;
import android.content.Context;

import com.uliana.squatchampion.data.database.RealmApi;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SampleModule {
    /**
     * Application instance.
     */
    private final Application application;

    public SampleModule(final Application application) {
        this.application = application;
    }

    /**
     * Provides application instance injecting.
     */
    @Provides
    @Singleton
    public Application provideApplication() {
        return application;
    }

    /**
     * Provides context instance injecting.
     */
    @Provides
    @Singleton
    public Context provideContext() {
        return application;
    }


    /**
     * Provides context instance injecting.
     */
    @Provides
    @Singleton
    public RealmApi provideRealmApi(Context context) {
        return new RealmApi(context);
    }


    @Provides
    @Singleton
    public Bus provideBus() {
        return new Bus(ThreadEnforcer.ANY);
    }

}
