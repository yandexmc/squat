package com.uliana.squatchampion.injection;

import com.uliana.squatchampion.SquatApplication;
import com.uliana.squatchampion.injection.module.SampleModule;
import lombok.Getter;

public final class Injector {

    @Getter
    private static ProjectComponent projectComponent;

    private Injector() {
    }

    /**
     * Creates an DaggerComponent if necessary
     */
    public static void initialize(final SquatApplication application) {
        synchronized (Injector.class) {
            if (projectComponent == null) {
                //DaggerProjectComponent generates in build directory after gradle assemble
                projectComponent = DaggerProjectComponent
                        .builder()
                        .sampleModule(new SampleModule(application))
                        .build();
            }
        }
    }

}
