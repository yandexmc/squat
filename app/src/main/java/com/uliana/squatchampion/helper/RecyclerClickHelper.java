package com.uliana.squatchampion.helper;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.uliana.squatchampion.R;

public final class RecyclerClickHelper {
    private final RecyclerView recyclerView;
    private OnItemClickListener onItemClickListener;
    private final View.OnClickListener localClickListener = makeClickListener();
    private OnItemLongClickListener onItemLongClickListener;
    private final View.OnLongClickListener localLongClickListener = makeLongClickListener();
    private final RecyclerView.OnChildAttachStateChangeListener localAttachListener = makeAttachListener();

    private RecyclerClickHelper(final RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.recyclerView.setTag(R.id.item_click_support, this);
        this.recyclerView.addOnChildAttachStateChangeListener(localAttachListener);
    }

    public static RecyclerClickHelper from(final RecyclerView view) {
        RecyclerClickHelper support = (RecyclerClickHelper) view.getTag(R.id.item_click_support);
        if (support == null) {
            support = new RecyclerClickHelper(view);
        }
        return support;
    }

    public static RecyclerClickHelper remove(final RecyclerView view) {
        final RecyclerClickHelper support = (RecyclerClickHelper) view.getTag(R.id.item_click_support);
        if (support != null) {
            support.detach(view);
        }
        return support;
    }

    public RecyclerClickHelper setOnItemClickListener(final OnItemClickListener listener) {
        onItemClickListener = listener;
        return this;
    }

    public RecyclerClickHelper setOnItemLongClickListener(final OnItemLongClickListener listener) {
        onItemLongClickListener = listener;
        return this;
    }

    private void detach(final RecyclerView view) {
        view.removeOnChildAttachStateChangeListener(localAttachListener);
        view.setTag(R.id.item_click_support, null);
    }

    @NonNull
    private View.OnClickListener makeClickListener() {
        return view -> {
            if (onItemClickListener != null) {
                final RecyclerView.ViewHolder holder = recyclerView.getChildViewHolder(view);
                onItemClickListener.onItemClicked(holder.getAdapterPosition());
            }
        };
    }

    @NonNull
    private View.OnLongClickListener makeLongClickListener() {
        return view -> {
            if (onItemLongClickListener != null) {
                final RecyclerView.ViewHolder holder = recyclerView.getChildViewHolder(view);
                return onItemLongClickListener.onItemLongClicked(holder.getAdapterPosition());
            }
            return false;
        };
    }

    @NonNull
    private RecyclerView.OnChildAttachStateChangeListener makeAttachListener() {
        return new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(final View view) {
                if (onItemClickListener != null) {
                    view.setOnClickListener(localClickListener);
                }
                if (onItemLongClickListener != null) {
                    view.setOnLongClickListener(localLongClickListener);
                }
            }

            @Override
            public void onChildViewDetachedFromWindow(final View view) {
                //do nothing
            }
        };
    }

    public interface OnItemClickListener {
        void onItemClicked(final int position);
    }

    public interface OnItemLongClickListener {
        boolean onItemLongClicked(int position);
    }

}
