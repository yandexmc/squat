package com.uliana.squatchampion;

import android.support.multidex.MultiDexApplication;

import com.uliana.squatchampion.injection.Injector;

import net.danlew.android.joda.JodaTimeAndroid;


@SuppressWarnings("unused")
public class SquatApplication extends MultiDexApplication {


    @Override
    public void onCreate() {
        super.onCreate();

        /*Dagger*/
        Injector.initialize(this);

        /*Joda-Time*/
        JodaTimeAndroid.init(this);

    }

}
