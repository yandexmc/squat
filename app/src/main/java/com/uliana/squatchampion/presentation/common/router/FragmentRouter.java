package com.uliana.squatchampion.presentation.common.router;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import com.uliana.squatchampion.presentation.common.BaseFragment;
import com.uliana.squatchampion.presentation.common.state.AnimationType;
import com.uliana.squatchampion.util.FragmentUtils;

import java.util.List;

public class FragmentRouter {

    private final FragmentManager fragmentManager;

    public FragmentRouter(final FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    protected void replaceFragment(final Fragment fragment,
                                   final boolean clearBackStack,
                                   final AnimationType type) {
        if (clearBackStack) {
            FragmentUtils.clearBackStack(fragmentManager);
        }
        FragmentUtils.replaceFragment(fragmentManager, fragment, type);
    }

    public String getRestoredFragmentName() {
        final int lastPosition = fragmentManager.getBackStackEntryCount() - 1;
        return fragmentManager.getBackStackEntryAt(lastPosition).getName();
    }

    public boolean handleBack() {
        if (fragmentManager == null) {
            return true;
        }

         /*THEN DELEGATE WORK TO FRAGMENT ITSELF*/
        final List<Fragment> fragments = fragmentManager.getFragments();
        for (final Fragment fragment : fragments) {
            if (fragment instanceof BaseFragment) {
                final BaseFragment baseFragment = (BaseFragment) fragment;
                if (!baseFragment.isFragmentPaused() && baseFragment.onBackPressed()) {
                    return true;
                }
            }
        }

        /*THEN HANDLE FRAGMENTS STATE*/
        if (fragmentManager.getBackStackEntryCount() > 1) {
            /*REMOVE FRAGMENT FROM STACK IF POSSIBLE*/
            fragmentManager.popBackStackImmediate();
            return true;
        }

        return false;
    }
}
