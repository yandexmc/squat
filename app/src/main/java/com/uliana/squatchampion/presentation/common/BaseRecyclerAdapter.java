package com.uliana.squatchampion.presentation.common;

import android.support.v7.widget.RecyclerView;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public abstract class BaseRecyclerAdapter<Model, ViewHolder extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<ViewHolder> {

    @Getter
    @Setter
    private List<Model> items;

    public BaseRecyclerAdapter() {
        super();
    }

    public Model getItem(final int position) {
        return items.get(position);
    }

    public void addItems(final List<Model> newItems) {
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
