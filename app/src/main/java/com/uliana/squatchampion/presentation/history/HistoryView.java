package com.uliana.squatchampion.presentation.history;

import com.uliana.squatchampion.domain.model.dto.SquatCategoryDto;
import com.uliana.squatchampion.domain.model.dto.SquatResultDto;
import com.uliana.squatchampion.presentation.common.view.BaseFragmentView;

import java.util.List;

public interface HistoryView extends BaseFragmentView {

    void showResultList(final List<SquatResultDto> exampleItems);
}
