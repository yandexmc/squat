package com.uliana.squatchampion.presentation.category;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uliana.squatchampion.R;
import com.uliana.squatchampion.domain.model.dto.SquatCategoryDto;
import com.uliana.squatchampion.presentation.common.BaseRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryListAdapter extends BaseRecyclerAdapter<SquatCategoryDto,CategoryListAdapter.CategoryHolder> {

    @Override
    public CategoryHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(final CategoryHolder holder, final int position) {
        final SquatCategoryDto item = getItem(position);
        holder.text.setText(item.getTitle());
    }

    public static class CategoryHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        protected TextView text;

        public CategoryHolder(final View source) {
            super(source);
            ButterKnife.bind(this, source);
        }
    }
}
