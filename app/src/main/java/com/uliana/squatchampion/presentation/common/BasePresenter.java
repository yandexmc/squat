package com.uliana.squatchampion.presentation.common;

import com.uliana.squatchampion.data.database.RealmApi;
import com.uliana.squatchampion.domain.model.event.AppearanceChangedEvent;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.state.AppearanceType;
import com.squareup.otto.Bus;


import javax.inject.Inject;

import lombok.Getter;
import lombok.Setter;

public abstract class BasePresenter<View, Router> {

    @Getter
    private Bus bus;

    @Getter
    @Setter
    private View view;

    @Getter
    @Setter
    private Router router;

    @Inject
    protected RealmApi realmApi;


    public BasePresenter() {
        inject();
        bus = Injector.getProjectComponent().bus();
    }

    protected abstract void inject();

    public void registerBus() {
        bus.register(this);
    }

    public void unregisterBus() {
        bus.unregister(this);
    }

    public void openRealm() {
        realmApi.open();
    }

    public void closeRealm() {
        realmApi.close();
    }

    public void onSetUpAppearance(final AppearanceType appearanceType) {
        getBus().post(new AppearanceChangedEvent(appearanceType));
    }

}
