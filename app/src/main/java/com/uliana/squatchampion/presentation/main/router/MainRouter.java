package com.uliana.squatchampion.presentation.main.router;

import com.uliana.squatchampion.domain.model.dto.SquatResultDto;

public interface MainRouter {
    void openHomePage();

    void openHistoryPage();

    void openCategoriesPage();
}
