package com.uliana.squatchampion.presentation.common.view;

import com.uliana.squatchampion.presentation.common.state.AppearanceType;

/**
 * Represents part of AuthActivity.
 * Part may be something over screen like Toolbar or SlideMenu.
 */
public interface BasePartView extends BaseView {
    void setUpAppearance(final AppearanceType type);
}
