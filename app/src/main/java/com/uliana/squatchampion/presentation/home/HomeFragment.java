package com.uliana.squatchampion.presentation.home;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.uliana.squatchampion.R;
import com.uliana.squatchampion.domain.model.dto.SquatResultDto;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BaseFragment;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.common.inflater.Layout;
import com.uliana.squatchampion.presentation.common.state.AppearanceType;
import com.uliana.squatchampion.util.DateUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

@Layout(id = R.layout.fragment_home)

@SuppressWarnings({"WeakerAccess", "unused"})
public class HomeFragment extends BaseFragment implements HomeView, SensorEventListener {

    private static final int MIN_SQUAT_TIME = 2000;
    public static final int VIBRATE_TIME = 200;

    @Inject
    protected HomePresenter homePresenter;

    private Vibrator vibrator;
    private SensorManager sensorManager;
    private Sensor accelerometerSensor;
    private Sensor pressureSensor;

    private boolean isStarted = false;

    private float dAccel = 0;
    private float dPressure = 0;
    private int count = -1;
    private List<Float> dateAccel = new ArrayList<>();
    private List<Float> datePressure = new ArrayList<>();
    private boolean isTimeDelayOver = true;
    private long lastSquatTime;


    @BindView(R.id.tv_count)
    TextView textViewCount;

    @BindView(R.id.btn_stop)
    protected TextView btnStop;

    @BindView(R.id.tv_category)
    protected TextView textViewCategory;

    @BindView(R.id.tv_day)
    protected TextView textViewDay;

    @BindView(R.id.tv_result)
    protected TextView textViewResult;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public AppearanceType getFragmentAppearance() {
        return AppearanceType.TOOLBAR_MENU;
    }

    @Override
    protected BasePresenter getPresenter() {
        return homePresenter;
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        homePresenter.readDayInfo();
    }

    @Override
    protected void onPostViewCreate(View view, Bundle savedInstanceState) {
        super.onPostViewCreate(view, savedInstanceState);


        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        if (sensors.size() > 0) {
            for (Sensor sensor : sensors) {
                switch (sensor.getType()) {
                    case Sensor.TYPE_ACCELEROMETER:
                        if (accelerometerSensor == null) accelerometerSensor = sensor;
                        break;

                    case Sensor.TYPE_PRESSURE:
                        if (pressureSensor == null) pressureSensor = sensor;
                        break;

                    default:
                        break;
                }
            }
        }
    }

    @Override
    public void onPause() {
        unRegisterLSensorsListeners();
        super.onPause();
    }

    @OnClick(R.id.tv_count)
    public void onLoginClicked() {
        if (!isStarted) {
            isStarted = true;
            btnStop.setVisibility(View.VISIBLE);
            registerLSensorsListeners();
        }
        increaseSquatCount();
    }

    @OnClick(R.id.btn_stop)
    public void onStopClicked() {
        unRegisterLSensorsListeners();
        isStarted = false;
        btnStop.setVisibility(View.GONE);
        homePresenter.writeResult(count);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        final int INDEX_OF_ACCEL_Z = 2;
        final float ACCEL_ACCURACY = 3;

        float[] values = event.values;
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER: {
                if (Math.abs(values[INDEX_OF_ACCEL_Z] - dAccel) > ACCEL_ACCURACY) {
                    dAccel = values[INDEX_OF_ACCEL_Z];
                    dateAccel.add(dAccel);
                    Log.d("Time", " " + (DateUtils.nowMillis() - lastSquatTime));
                    if (dateAccel.size() > 3 && DateUtils.nowMillis() - lastSquatTime > MIN_SQUAT_TIME) {
                        if (checkPeak(dateAccel)) {
                            dateAccel = new ArrayList<>();
                            Log.d("Count Accel", " " +
                                    count);
                            increaseSquatCount();
                            lastSquatTime = DateUtils.nowMillis();
                        }
                    }
                }
                break;
            }
            case Sensor.TYPE_PRESSURE: {
                if (Math.abs(event.values[0] - dPressure) > 0.02f) {
                    dPressure = event.values[0];
                    datePressure.add(dPressure);

                    if (datePressure.size() > 3 && DateUtils.nowMillis() - lastSquatTime > MIN_SQUAT_TIME) {
                        if (checkPeak(datePressure)) {
                            datePressure = new ArrayList<>();
                            Log.d("Count Pressure", " " +
                                    count);
                            increaseSquatCount();
                            lastSquatTime = DateUtils.nowMillis();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private boolean checkPeak(List<Float> date) {
        return date.get(date.size() - 2) > date.get(date.size() - 3) && date.get(date.size() - 2) < date.get(date.size() - 1);
    }

    private void increaseSquatCount() {
        count++;
        textViewCount.setText(String.valueOf(count));
        vibrator.vibrate(VIBRATE_TIME);
    }

    private void registerLSensorsListeners() {
        if (accelerometerSensor != null) {
            sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_FASTEST);
        }
        if (pressureSensor != null) {
            sensorManager.registerListener(this, pressureSensor, SensorManager.SENSOR_DELAY_FASTEST);
        }
    }

    private void unRegisterLSensorsListeners() {
        if (accelerometerSensor != null) {
            sensorManager.unregisterListener(this, accelerometerSensor);
        }
        if (pressureSensor != null) {
            sensorManager.unregisterListener(this, pressureSensor);
        }
    }


    @Override
    public void showDayInfo(SquatResultDto squatResultDto) {
        textViewCategory.setText(squatResultDto.getCategory());
        textViewDay.setText(getString(R.string.day_format, squatResultDto.getDayNumber() + 1));
        textViewResult.setText(getString(R.string.round_format, squatResultDto.getRoundsCount()));
    }
}
