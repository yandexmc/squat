package com.uliana.squatchampion.presentation.category;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;

import com.uliana.squatchampion.R;
import com.uliana.squatchampion.domain.model.dto.SquatCategoryDto;
import com.uliana.squatchampion.helper.RecyclerClickHelper;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BaseFragment;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.common.inflater.Layout;
import com.uliana.squatchampion.presentation.common.state.AppearanceType;
import com.uliana.squatchampion.util.DialogUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

@Layout(id = R.layout.fragment_category_list)
@SuppressWarnings({"WeakerAccess", "unused"})
public class CategoryListFragment extends BaseFragment implements CategoryLisView {

    private static final String STATE_ITEMS_KEY = "EXAMPLE_ITEMS_KEY";

    @Inject
    protected CategoryListPresenter categoryListPresenter;

    @BindView(R.id.example_recycler)
    protected RecyclerView recyclerView;

    private CategoryListAdapter categoryListAdapter;
    private List<SquatCategoryDto> categoryDtoList;

    public static CategoryListFragment newInstance() {
        return new CategoryListFragment();
    }

    @Override
    public AppearanceType getFragmentAppearance() {
        return AppearanceType.TOOLBAR_NO_MENU;
    }

    @Override
    protected void onPostViewCreate(final View view, final Bundle savedInstanceState) {
        super.onPostViewCreate(view, savedInstanceState);

        categoryListAdapter = new CategoryListAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(categoryListAdapter);

        RecyclerClickHelper.from(recyclerView).setOnItemClickListener(position -> {
            categoryListPresenter.handleItemClicked(categoryListAdapter.getItem(position));
        });
    }

    @Override
    protected BasePresenter getPresenter() {
        return categoryListPresenter;
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (categoryDtoList != null) {
            showCategoryList(categoryDtoList);
        } else {
            categoryListPresenter.readCategories();
        }
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        if (categoryListAdapter != null && categoryListAdapter.getItems() != null && !categoryListAdapter.getItems().isEmpty()) {
            outState.putParcelableArrayList(STATE_ITEMS_KEY, new ArrayList<>(categoryListAdapter.getItems()));
        }
    }

    @Override
    protected void tryRestoreState(@NonNull final Bundle savedInstanceState) {
        super.tryRestoreState(savedInstanceState);
        if (savedInstanceState.containsKey(STATE_ITEMS_KEY)) {
            categoryDtoList = savedInstanceState.getParcelableArrayList(STATE_ITEMS_KEY);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (categoryListAdapter != null) {
            categoryDtoList = categoryListAdapter.getItems();
        }
    }

    @Override
    public void showCategoryList(final List<SquatCategoryDto> exampleItems) {
        categoryListAdapter.setItems(exampleItems);
    }

    @Override
    public void showAddCategoryDialog() {
        AlertDialog editTextDialog = DialogUtils.createEditTextDialog(getContext(),
                getString(R.string.dialog_add_category_tittle), true,
                text -> categoryListPresenter.handleCategoryTitleEntered(text));
        editTextDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        editTextDialog.show();
    }


    @OnClick(R.id.floating_button)
    public void addButtonClicked() {
        categoryListPresenter.handleAddButtonClicked();
    }
}
