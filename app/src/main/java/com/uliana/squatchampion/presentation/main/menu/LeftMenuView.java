package com.uliana.squatchampion.presentation.main.menu;

import android.app.Activity;
import com.uliana.squatchampion.presentation.common.view.BasePartView;

public interface LeftMenuView extends BasePartView {
    boolean handleBack();

    void initialize(Activity activity);

    void toggleMenu();

    void openMenu();

    void closeMenu();

    void lockMenu();

    void unlockMenu();
}
