package com.uliana.squatchampion.presentation.main;

import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.main.router.MainRouter;

import javax.inject.Inject;

public class MainPresenter extends BasePresenter<MainView, MainRouter> {
    @Inject
    public MainPresenter() {
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

}
