package com.uliana.squatchampion.presentation.home;

import com.uliana.squatchampion.domain.model.dto.SquatResultDto;
import com.uliana.squatchampion.presentation.common.view.BaseFragmentView;

public interface HomeView extends BaseFragmentView {

    public void showDayInfo(SquatResultDto squatResultDto);

}
