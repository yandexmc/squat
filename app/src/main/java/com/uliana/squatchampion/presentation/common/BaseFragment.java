package com.uliana.squatchampion.presentation.common;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.uliana.squatchampion.presentation.common.view.BaseFragmentView;
import com.uliana.squatchampion.presentation.common.state.AppearanceType;
import com.uliana.squatchampion.util.AppUtils;
import lombok.Getter;


@SuppressWarnings({"WeakerAccess", "SameParameterValue", "EmptyMethod", "unused", "UnusedParameters"})
public abstract class BaseFragment extends Fragment implements BaseFragmentView {

    @Getter
    private boolean fragmentPaused = true;

    private Unbinder unbinder;

    protected abstract BasePresenter getPresenter();

    protected abstract void inject();

    public AppearanceType getFragmentAppearance() {
        return AppearanceType.NO_INFLUENCE;
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
        if (arguments != null) {
            onHandleArguments(arguments);
        }

        if (savedInstanceState != null) {
            tryRestoreState(savedInstanceState);
        }
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return AppUtils.inflateLayout(inflater, container, this);
    }

    @Override
    public void onViewStateRestored(@Nullable final Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            tryRestoreState(savedInstanceState);
        }
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        onPostViewCreate(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getPresenter() == null) {
            inject();
        }
        //noinspection unchecked
        getPresenter().setView(this);

        setRetainInstance(true);
        //set up toolbar main activity
        final BaseActivity baseActivity = getBaseActivity();
        //noinspection unchecked
        getPresenter().setRouter(baseActivity.getFragmentRouter());
    }

    @Override
    public void onResume() {
        super.onResume();
        fragmentPaused = false;
        getPresenter().registerBus();
        getPresenter().onSetUpAppearance(getFragmentAppearance());
        getPresenter().openRealm();
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        fragmentPaused = true;
        getPresenter().unregisterBus();
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().closeRealm();
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        super.onDestroyView();
        //noinspection unchecked
        getPresenter().setRouter(null);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    protected void onHandleArguments(@NonNull final Bundle arguments) {
        /* This method need to be implemented in child fragments.
         * Called once in onCreate() method if arguments bundle not null. */
    }


    /**
     * Extended Fragment lifecycle for ButterKnife.
     * This method need t o be implemented in child fragments.
     * Method called in onViewCreate() just after ButterKnife was initialized.
     */
    protected void onPostViewCreate(final View view, final Bundle savedInstanceState) {
    }

    /**
     * common logic of savedInstanceState parsing.
     * This method called twice:
     * - onCreate() if savedInstanceState not null.
     * - onViewStateRestore() if savedInstanceState not null.
     */
    protected void tryRestoreState(@NonNull final Bundle savedInstanceState) {
    }

    /**
     * Method for handling fragment specific back press event.
     *
     * @return boolean (true - stop handling, false - continue handling)
     */
    @SuppressWarnings("SameReturnValue")
    public boolean onBackPressed() {
        return false;
    }

    protected void performBackPress() {
        getBaseActivity().onBackPressed();
    }

    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public void showToast(final CharSequence text) {
        getBaseActivity().showToast(text);
    }

    @Override
    public void showToast(@StringRes final int stringId, final Object... args) {
        getBaseActivity().showToast(stringId, args);
    }
}
