package com.uliana.squatchampion.presentation.main.menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.uliana.squatchampion.R;
import com.uliana.squatchampion.domain.model.dto.MenuItemDto;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class MenuItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MenuItemDto> categories;

    MenuItemAdapter() {
        super();
        categories =
                Arrays.asList(MenuItemDto.HOME, MenuItemDto.HISTORY, MenuItemDto.CATEGORIES);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final Context context = parent.getContext();
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.item_main_menu, parent, false);
        return new MenuItemHolder(view);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final MenuItemDto item = getItem(position);
        final MenuItemHolder menuHolder = (MenuItemHolder) holder;
        menuHolder.text.setText(item.getStringId());
        menuHolder.icon.setImageResource(item.getIconId());
    }


    @Override
    public int getItemCount() {
        return categories.size();
    }

    public MenuItemDto getItem(int position) {
        return categories.get(position);
    }


    static class MenuItemHolder extends RecyclerView.ViewHolder {
        View root;
        @BindView(R.id.icon)
        ImageView icon;
        @BindView(R.id.text)
        TextView text;

        MenuItemHolder(final View source) {
            super(source);
            ButterKnife.bind(this, source);
            root = source;
            text.setOnTouchListener((v, event) -> root.onTouchEvent(event));
        }

    }
}
