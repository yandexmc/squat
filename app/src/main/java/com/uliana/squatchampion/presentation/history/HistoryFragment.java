package com.uliana.squatchampion.presentation.history;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.uliana.squatchampion.R;
import com.uliana.squatchampion.domain.model.dto.SquatResultDto;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BaseFragment;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.common.inflater.Layout;
import com.uliana.squatchampion.presentation.common.state.AppearanceType;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

@Layout(id = R.layout.fragment_history)
@SuppressWarnings({"WeakerAccess", "unused"})
public class HistoryFragment extends BaseFragment implements HistoryView {

    @Inject
    protected HistoryPresenter historyPresenter;

    @BindView(R.id.recycler_history)
    protected RecyclerView recyclerViewHistory;

    private HistoryListAdapter historyListAdapter;
    private List<SquatResultDto> resultDtoList;

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    @Override
    public AppearanceType getFragmentAppearance() {
        return AppearanceType.TOOLBAR_MENU;
    }


    @Override
    protected BasePresenter getPresenter() {
        return historyPresenter;
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (resultDtoList != null) {
            showResultList(resultDtoList);
        } else {
            historyPresenter.readResults();
        }
    }


    @Override
    protected void onPostViewCreate(View view, Bundle savedInstanceState) {
        super.onPostViewCreate(view, savedInstanceState);

        historyListAdapter = new HistoryListAdapter();
        recyclerViewHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerViewHistory.setAdapter(historyListAdapter);
    }


    @Override
    public void showResultList(List<SquatResultDto> items) {
        historyListAdapter.setItems(items);
    }
}
