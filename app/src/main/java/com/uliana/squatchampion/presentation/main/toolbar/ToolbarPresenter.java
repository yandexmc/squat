package com.uliana.squatchampion.presentation.main.toolbar;

import com.uliana.squatchampion.domain.model.event.AppearanceChangedEvent;
import com.uliana.squatchampion.domain.model.event.ToggleDrawerEvent;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.main.router.MainRouter;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

public class ToolbarPresenter extends BasePresenter<ToolbarView, MainRouter> {

    @Inject
    public ToolbarPresenter() {
        super();
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    void handleRootClick() {

    }

    void handleToggleClick() {
        getBus().post(new ToggleDrawerEvent());
    }

    @Subscribe
    public void onAppearanceChanged(final AppearanceChangedEvent event) {
        getView().setUpAppearance(event.getAppearanceType());
    }
}
