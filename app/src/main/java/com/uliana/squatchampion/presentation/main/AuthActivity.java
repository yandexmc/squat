package com.uliana.squatchampion.presentation.main;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.uliana.squatchampion.R;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BaseActivity;
import com.uliana.squatchampion.presentation.common.inflater.Layout;
import com.uliana.squatchampion.presentation.main.router.MainRouter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.Observer;
import rx.Subscription;

@Layout(id = R.layout.fragment_home)
public class AuthActivity extends BaseActivity<MainRouter> implements SensorEventListener {
    private static final int REFRESH_CHAT_DELAY = 2;


    private SensorManager mSensorManager;
    private Sensor mAccelerometerSensor;
    private Sensor pressureSensor;

    private Subscription subscriptionChat;


    private SensorManager sensorMan;
    private Sensor accelerometer;

    private float[] mGravity;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;


    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        final FragmentManager fragmentManager = getSupportFragmentManager();
//        /*SET UP VIPER ROUTER*/
//        authRouter = new AuthRouterImpl(fragmentManager);

        // sensorMan = (SensorManager)getSystemService(SENSOR_SERVICE);
        // accelerometer = sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        if (sensors.size() > 0) {
            for (Sensor sensor : sensors) {
                switch (sensor.getType()) {
                    case Sensor.TYPE_ACCELEROMETER:
                        if (mAccelerometerSensor == null) mAccelerometerSensor = sensor;
                        break;

                    case Sensor.TYPE_PRESSURE:
                        if (pressureSensor == null) pressureSensor = sensor;
                        break;

                    default:
                        break;
                }
            }
        }
    }

    @Override
    public MainRouter getFragmentRouter() {
        return null;
    }


//    @Override
//    public AuthRouter getFragmentRouter() {
//        return authRouter;
//    }
//
//    @Override
//    public void onBackPressed() {
//        /*PASS CONTROL TO MAIN ROUTER*/
//        if (authRouter.handleBack()) {
//            return;
//        }
//
//        /*CLOSE APPLICATION IF THERE ARE NO FRAGMENTS TO REMOVE*/
//        finish();
//    }

    float dAccel = 0;
    float dPressure = 0;
    int count = 0;
    List<Float> dateAccel = new ArrayList<>();
    List<Float> datePressure = new ArrayList<>();
    private boolean isTimeDelayOver = true;

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;

        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER: {
                if (Math.abs(event.values[2] - dAccel) > 3) {
                    dAccel = event.values[2];
                    dateAccel.add(dAccel);
                    Log.d("Added", (String.format("%1.2f", dAccel)));
                    if (dateAccel.size() > 3 && isTimeDelayOver) {
                        if (dateAccel.get(dateAccel.size() - 2) > dateAccel.get(dateAccel.size() - 3) && dateAccel.get(dateAccel.size() - 2) < dateAccel.get(dateAccel.size() - 1)) {
                            dateAccel = new ArrayList<>();
                            Log.d("Count Accel", " " +
                                    count);
                            count++;
                           // textView.setText(String.valueOf(count));
                            startDelay();
                        }
                    }
                }

//                mGravity = event.values.clone();
//                // Shake detection
//                float x = mGravity[0];
//                float y = mGravity[1];
//                float z = mGravity[2];
//                mAccelLast = mAccelCurrent;
//                mAccelCurrent = (float) Math.sqrt(x * x + y * y + z * z);
//                float delta = mAccelCurrent - mAccelLast;
//                mAccel = mAccel * 0.9f + delta;
//                // Make this higher or lower according to how much
//                // motion you want to detect
//                if (mAccel > 2) {
//                    date.add(mAccel);
//                    if (date.size() > 3 && isTimeDelayOver) {
//                        if (date.get(date.size() - 2) > date.get(date.size() - 3) && date.get(date.size() - 2) < date.get(date.size() - 1)) {
//                            date = new ArrayList<>();
//                            Log.d("Count", " " +
//                                    count);
//                            count++;
//                            textView.setText(String.valueOf(count));
//                            startDelay();
//                        }
//                    }
//                }
                break;
            }
            case Sensor.TYPE_PRESSURE: {
                if (Math.abs(event.values[0] - dPressure) > 0.02f) {
                    dPressure = event.values[0];
                    datePressure.add(dPressure);
                    Log.d("Added", (String.format("%1.2f", dPressure)));
                    if (datePressure.size() > 3 && isTimeDelayOver) {
                        if (datePressure.get(datePressure.size() - 2) > datePressure.get(datePressure.size() - 3) && datePressure.get(datePressure.size() - 2) < datePressure.get(datePressure.size() - 1)) {
                            datePressure = new ArrayList<>();
                            Log.d("Count Pressure", " " +
                                    count);
                            count++;
                          //  textView.setText(String.valueOf(count));
                            startDelay();
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void startDelay() {
        isTimeDelayOver = false;
        subscriptionChat = Observable.timer(REFRESH_CHAT_DELAY, TimeUnit.SECONDS)
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onCompleted() {
                        isTimeDelayOver = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Long aLong) {
                        isTimeDelayOver = true;
                    }
                });
    }
}
