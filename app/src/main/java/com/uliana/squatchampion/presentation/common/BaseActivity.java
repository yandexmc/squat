package com.uliana.squatchampion.presentation.common;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.uliana.squatchampion.util.AppUtils;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

@SuppressWarnings({"WeakerAccess", "SameParameterValue", "unused"})
public abstract class BaseActivity<Router> extends AppCompatActivity {


    protected abstract void inject();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        /*INITIALIZE INJECTORS*/
        setContentView(AppUtils.inflateLayout(LayoutInflater.from(this), null, this));

        /* DI */
        inject();
        ButterKnife.bind(this);

    }

    @Override
    protected void attachBaseContext(final Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void hideKeyboard() {
        final View view = getCurrentFocus();
        if (view != null) {
            final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public abstract Router getFragmentRouter();


    public void showToast(final CharSequence text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public void showToast(@StringRes final int stringId, final Object... args) {
        showToast(getString(stringId, args));
    }
}
