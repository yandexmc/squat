package com.uliana.squatchampion.presentation.main.toolbar;

import android.support.annotation.StringRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.uliana.squatchampion.R;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.state.AppearanceType;
import com.uliana.squatchampion.presentation.main.MainActivity;
import com.uliana.squatchampion.util.AppUtils;

import javax.inject.Inject;

public class ToolbarHolder implements ToolbarView {

    private final ActionBarDrawerToggle drawerToggle;

    private final ActionBar supportActionBar;

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    /*
    @BindView(R.id.toolbar_menu_item)
    public ImageView menuIcon;

    @BindView(R.id.toolbar_back_item)
    public ImageView backIcon;
    */

    @Inject
    ToolbarPresenter toolbarPresenter;

    private Unbinder viewUnbinder;

    public ToolbarHolder(final MainActivity activity) {
        //Inject
        Injector.getProjectComponent().inject(this);
        ButterKnife.bind(this, activity);

        //Set as action abr
        activity.setSupportActionBar(toolbar);
        supportActionBar = activity.getSupportActionBar();
        assert supportActionBar != null;

        //Initialize presenter
        toolbarPresenter.setView(this);
        toolbarPresenter.setRouter(activity.getFragmentRouter());

        //Initialize toggle
        final DrawerLayout drawerLayout = ButterKnife.findById(activity, R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(activity, drawerLayout, android.R.string.untitled, android.R.string.untitled);
        drawerLayout.addDrawerListener(drawerToggle);
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        drawerToggle.syncState();
        toolbar.setNavigationOnClickListener(v -> toolbarPresenter.handleToggleClick());
    }

    public void onResume() {
        toolbarPresenter.registerBus();
    }

    public void onPause() {
        toolbarPresenter.unregisterBus();
    }

    public void onDestroy() {
        if (viewUnbinder != null) {
            viewUnbinder.unbind();
            viewUnbinder = null;
        }
    }

    private void hideToolbar() {
        AppUtils.changeViewVisibility(false, toolbar);
    }

    private void showToolbar(final boolean home) {
        AppUtils.changeViewVisibility(true, toolbar);
        supportActionBar.setHomeButtonEnabled(home);
    }

    @OnClick(R.id.toolbar)
    protected void onToolbarClick() {
        toolbarPresenter.handleRootClick();
    }

    @Override
    public void setUpAppearance(final AppearanceType type) {
        switch (type) {
            case TOOLBAR_MENU:
                showToolbar(true);
                break;
            case TOOLBAR_NO_MENU:
                showToolbar(false);
                break;
            case FULLSCREEN_MODAL:
                hideToolbar();
                break;
            case NO_INFLUENCE:
                //do nothing
                break;
            default:
                break;
        }
    }

    @Override
    public void showToast(final CharSequence text) {
        Toast.makeText(toolbar.getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(@StringRes final int stringId, final Object... args) {
        showToast(toolbar.getContext().getString(stringId, args));
    }
}
