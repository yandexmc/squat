package com.uliana.squatchampion.presentation.category;

import com.uliana.squatchampion.domain.model.dto.SquatCategoryDto;
import com.uliana.squatchampion.presentation.common.view.BaseFragmentView;

import java.util.List;

public interface CategoryLisView extends BaseFragmentView {
    void showCategoryList(final List<SquatCategoryDto> exampleItems);

    void showAddCategoryDialog();
}
