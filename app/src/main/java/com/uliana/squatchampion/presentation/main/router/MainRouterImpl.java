package com.uliana.squatchampion.presentation.main.router;

import android.support.v4.app.FragmentManager;

import com.uliana.squatchampion.domain.model.dto.SquatResultDto;
import com.uliana.squatchampion.presentation.category.CategoryListFragment;
import com.uliana.squatchampion.presentation.common.router.FragmentRouter;
import com.uliana.squatchampion.presentation.common.state.AnimationType;
import com.uliana.squatchampion.presentation.history.HistoryFragment;
import com.uliana.squatchampion.presentation.home.HomeFragment;

public class MainRouterImpl extends FragmentRouter implements MainRouter {

    public MainRouterImpl(final FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public void openHomePage() {
        replaceFragment(HomeFragment.newInstance(), true, AnimationType.FADE);
    }

    @Override
    public void openHistoryPage() {
        replaceFragment(HistoryFragment.newInstance(), true, AnimationType.FADE);
    }

    @Override
    public void openCategoriesPage() {
        replaceFragment(CategoryListFragment.newInstance(), true, AnimationType.FROM_RIGHT);
    }


}
