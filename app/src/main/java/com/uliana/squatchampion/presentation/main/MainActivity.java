package com.uliana.squatchampion.presentation.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.uliana.squatchampion.R;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.common.BasePresenterActivity;
import com.uliana.squatchampion.presentation.common.inflater.Layout;
import com.uliana.squatchampion.presentation.main.menu.LeftMenuView;
import com.uliana.squatchampion.presentation.main.router.MainRouter;
import com.uliana.squatchampion.presentation.main.router.MainRouterImpl;
import com.uliana.squatchampion.presentation.main.toolbar.ToolbarHolder;
import com.uliana.squatchampion.util.AppUtils;

import javax.inject.Inject;

@Layout(id = R.layout.activity_main)
public class MainActivity extends BasePresenterActivity<MainRouter> implements MainView {

    public static final String EXTRA_VALUE_KEY = "extra_value_key";

    public static Intent newIntent(final Context context, final String extraValue) {
        final Bundle extras = new Bundle();
        extras.putString(EXTRA_VALUE_KEY, extraValue);
        return AppUtils.withExtras(new Intent(context, MainActivity.class), extras);
    }

    @Inject
    MainPresenter mainPresenter;

    private MainRouterImpl mainRouter;

    private LeftMenuView leftMenuView;
    private ToolbarHolder toolbarHolder;

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return mainPresenter;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final FragmentManager fragmentManager = getSupportFragmentManager();
        /*SET UP VIPER ROUTER*/
        mainRouter = new MainRouterImpl(fragmentManager);

        /*SET UP MENU*/
        leftMenuView = (LeftMenuView) fragmentManager.findFragmentById(R.id.navigation_drawer);
        leftMenuView.initialize(this);

        /*SET UP TOOLBAR*/
        toolbarHolder = new ToolbarHolder(this);

        if (savedInstanceState == null) {
            mainRouter.openHomePage();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        toolbarHolder.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        toolbarHolder.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        toolbarHolder.onDestroy();
    }

    @Override
    public MainRouter getFragmentRouter() {
        return mainRouter;
    }

    @Override
    public void onBackPressed() {
        /*FIRST OF ALL HANDLE NAVIGATION DRAWER*/
        if (leftMenuView.handleBack()) {
            return;
        }

        /*PASS CONTROL TO MAIN ROUTER*/
        if (mainRouter.handleBack()) {
            return;
        }

        /*CLOSE APPLICATION IF THERE ARE NO FRAGMENTS TO REMOVE*/
        finish();
    }

}
