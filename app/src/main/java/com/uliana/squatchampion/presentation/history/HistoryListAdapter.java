
package com.uliana.squatchampion.presentation.history;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uliana.squatchampion.R;
import com.uliana.squatchampion.domain.model.dto.SquatResultDto;
import com.uliana.squatchampion.presentation.common.BaseRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryListAdapter extends BaseRecyclerAdapter<SquatResultDto, HistoryListAdapter.HistoryHolder> {

    @Override
    public HistoryHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_result, parent, false);
        return new HistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(final HistoryHolder holder, final int position) {
        final SquatResultDto item = getItem(position);
        holder.textViewTitle.setText(holder.textViewTitle.getContext()
                .getString(R.string.result_title_format, position + 1, item.getCategory()));
        holder.textViewDate.setText(item.getDate());
        holder.textViewResult.setText(item.getResults());
        holder.textViewCount.setText(String.valueOf(item.getCount()));
    }

    public static class HistoryHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        protected TextView textViewTitle;

        @BindView(R.id.tv_date)
        protected TextView textViewDate;

        @BindView(R.id.tv_result)
        protected TextView textViewResult;

        @BindView(R.id.tv_total_count)
        protected TextView textViewCount;


        public HistoryHolder(final View source) {
            super(source);
            ButterKnife.bind(this, source);
        }
    }
}
