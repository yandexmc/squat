package com.uliana.squatchampion.presentation.home;

import com.uliana.squatchampion.data.database.model.RealmSquatCategory;
import com.uliana.squatchampion.data.database.model.RealmSquatResult;
import com.uliana.squatchampion.data.shared_preferences.Preferences;
import com.uliana.squatchampion.domain.model.dto.SquatResultDto;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.main.router.MainRouter;
import com.uliana.squatchampion.util.DateUtils;

import javax.inject.Inject;

public class HomePresenter extends BasePresenter<HomeView, MainRouter> {

    @Inject
    Preferences preferences;

    @Inject
    public HomePresenter() {
        super();
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    public void readDayInfo() {
        SquatResultDto squatResultDto = realmApi.getTodayResult();
        if (squatResultDto == null) {
            if (preferences.readSelectedCategory().isEmpty()) {
                getRouter().openCategoriesPage();
            } else {
                squatResultDto = new SquatResultDto(DateUtils.today(), preferences.readSelectedCategory(), "", 0, 0, 0);
                getView().showDayInfo(squatResultDto);
            }
        } else {
            getView().showDayInfo(squatResultDto);
        }
    }

    public void writeResult(int count) {
        realmApi.saveResult(new RealmSquatResult(DateUtils.nowMillis(),
                new RealmSquatCategory(preferences.readSelectedCategory()), count));
        preferences.writeSelectedCategory("");
        getRouter().openHistoryPage();
    }


}
