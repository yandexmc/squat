package com.uliana.squatchampion.presentation.common.state;

public enum AnimationType {
    FROM_BOTTOM,
    FADE,
    FROM_RIGHT,
    NO_ANIMATION,
}
