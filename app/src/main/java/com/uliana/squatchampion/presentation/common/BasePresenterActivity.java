package com.uliana.squatchampion.presentation.common;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.uliana.squatchampion.presentation.common.state.AppearanceType;
import com.uliana.squatchampion.presentation.common.view.BasePartView;

@SuppressWarnings({"WeakerAccess", "SameParameterValue", "unused"})
public abstract class BasePresenterActivity<Router> extends BaseActivity<Router> implements BasePartView {

    protected abstract BasePresenter getPresenter();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //noinspection unchecked
        getPresenter().setView(this);
        //noinspection unchecked
        getPresenter().setRouter(getFragmentRouter());
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().registerBus();
        getPresenter().openRealm();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPresenter().unregisterBus();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //noinspection unchecked
        getPresenter().setView(null);
        //noinspection unchecked
        getPresenter().setRouter(null);
        getPresenter().closeRealm();
    }

    @Override
    public void setUpAppearance(final AppearanceType type) {
        //do nothing
    }

    @Override
    public void showToast(final CharSequence text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(@StringRes final int stringId, final Object... args) {
        showToast(getString(stringId, args));
    }
}
