package com.uliana.squatchampion.presentation.history;

import com.uliana.squatchampion.data.database.model.RealmSquatCategory;
import com.uliana.squatchampion.data.database.model.RealmSquatResult;
import com.uliana.squatchampion.data.shared_preferences.Preferences;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.main.router.MainRouter;

import java.util.Calendar;

import javax.inject.Inject;

public class HistoryPresenter extends BasePresenter<HistoryView, MainRouter> {

    @Inject
    Preferences preferences;


    @Inject
    public HistoryPresenter() {
        super();
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    public void readResults() {
        getView().showResultList(realmApi.loadResults());
    }
}
