package com.uliana.squatchampion.presentation.common.view;

import android.support.annotation.StringRes;

/**
 *
 */
public interface BaseView {
    void showToast(final CharSequence text);

    void showToast(@StringRes final int stringId, Object... args);
}
