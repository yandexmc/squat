package com.uliana.squatchampion.presentation.main.menu;

import com.uliana.squatchampion.domain.model.dto.MenuItemDto;
import com.uliana.squatchampion.domain.model.event.AppearanceChangedEvent;
import com.uliana.squatchampion.domain.model.event.ToggleDrawerEvent;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.main.router.MainRouter;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

public class LeftMenuPresenter extends BasePresenter<LeftMenuView, MainRouter> {

    @Inject
    public LeftMenuPresenter() {
        super();
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    public void onMenuClicked() {
        getView().closeMenu();
    }

    void handleMenuClick(final MenuItemDto menuItem) {
        switch (menuItem) {
            case HOME:
                getRouter().openHomePage();
                break;
            case HISTORY:
                getRouter().openHistoryPage();
                break;
            case CATEGORIES:
                getRouter().openCategoriesPage();
                break;
            default:
                break;
        }
        getView().closeMenu();
    }

    @Subscribe
    public void onAppearanceChanged(final AppearanceChangedEvent event) {
        getView().setUpAppearance(event.getAppearanceType());
    }

    @Subscribe
    public void onDrawerToggleEvent(final ToggleDrawerEvent event) {
        getView().toggleMenu();
    }
}
