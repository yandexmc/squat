package com.uliana.squatchampion.presentation.common.state;

public enum AppearanceType {
    TOOLBAR_MENU,
    TOOLBAR_NO_MENU,
    FULLSCREEN_MODAL,
    NO_INFLUENCE,
}
