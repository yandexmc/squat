package com.uliana.squatchampion.presentation.category;

import com.uliana.squatchampion.R;
import com.uliana.squatchampion.data.database.model.RealmSquatCategory;
import com.uliana.squatchampion.data.shared_preferences.Preferences;
import com.uliana.squatchampion.domain.model.dto.SquatCategoryDto;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.main.router.MainRouter;

import javax.inject.Inject;

public class CategoryListPresenter extends BasePresenter<CategoryLisView, MainRouter> {

    @Inject
    Preferences preferences;

    @Inject
    public CategoryListPresenter() {
        super();
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    public void readCategories() {
        getView().showCategoryList(realmApi.loadCategories());
    }


    public void handleItemClicked(final SquatCategoryDto squatCategoryDto) {
        if (realmApi.getTodayResult() != null) {
            getView().showToast(R.string.techique_selected);
        } else {
            preferences.writeSelectedCategory(squatCategoryDto.getTitle());
        }
        getRouter().openHomePage();
    }

    public void handleAddButtonClicked() {
        getView().showAddCategoryDialog();
    }

    public void handleCategoryTitleEntered(String categoryName) {
        realmApi.saveCategory(new RealmSquatCategory(categoryName));
        readCategories();
    }
}
