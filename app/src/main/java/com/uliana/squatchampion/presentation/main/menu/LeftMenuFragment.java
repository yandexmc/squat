package com.uliana.squatchampion.presentation.main.menu;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.uliana.squatchampion.R;
import com.uliana.squatchampion.domain.model.dto.MenuItemDto;
import com.uliana.squatchampion.helper.RecyclerClickHelper;
import com.uliana.squatchampion.injection.Injector;
import com.uliana.squatchampion.presentation.common.inflater.Layout;
import com.uliana.squatchampion.presentation.common.BaseFragment;
import com.uliana.squatchampion.presentation.common.BasePresenter;
import com.uliana.squatchampion.presentation.common.state.AppearanceType;

import javax.inject.Inject;

@Layout(id = R.layout.fragment_left_menu)
@SuppressWarnings("WeakerAccess")
public class LeftMenuFragment extends BaseFragment implements LeftMenuView {

    @Inject
    protected LeftMenuPresenter leftMenuPresenter;

    @BindView(R.id.recycler)
    RecyclerView menuRecycler;

    private ViewGroup fragmentView;
    private DrawerLayout drawerLayout;
    private MenuItemAdapter menuAdapter = new MenuItemAdapter();

    @Override
    public AppearanceType getFragmentAppearance() {
        return AppearanceType.NO_INFLUENCE; //whenever this fragment instantiated - it should not change toolbar, etc..
    }

    @Override
    protected void inject() {
        Injector.getProjectComponent().inject(this);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    protected void onPostViewCreate(final View view, final Bundle savedInstanceState) {
        super.onPostViewCreate(view, savedInstanceState);
        menuRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        menuRecycler.setHasFixedSize(true);
        menuRecycler.setAdapter(menuAdapter);

        RecyclerClickHelper.from(menuRecycler).setOnItemClickListener(position -> {
            final MenuItemDto item = menuAdapter.getItem(position);
            leftMenuPresenter.handleMenuClick(item);
        });
    }

    public void disableDrawerSwipe(final Boolean close) {
        if (close != null) {
            drawerLayout.setDrawerLockMode(close ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_LOCKED_OPEN);
        } else if (drawerLayout.isDrawerOpen(fragmentView)) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    @Override
    public void initialize(final Activity activity) {
        drawerLayout = ButterKnife.findById(activity, R.id.drawer_layout);
        fragmentView = ButterKnife.findById(activity, R.id.navigation_drawer);
        disableDrawerSwipe(null);
    }

    @Override
    public void openMenu() {
        drawerLayout.openDrawer(fragmentView);
    }

    @Override
    public void closeMenu() {
        drawerLayout.closeDrawer(fragmentView);
    }

    @Override
    public void lockMenu() {
        disableDrawerSwipe(null);
    }

    @Override
    public void unlockMenu() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public boolean handleBack() {
        if (isDrawerOpened()) {
            closeMenu();
            return true;
        }
        return false;
    }

    @Override
    public void toggleMenu() {
        if (isDrawerOpened()) {
            closeMenu();
        } else {
            openMenu();
        }
    }

    private boolean isDrawerOpened() {
        return drawerLayout.isDrawerOpen(Gravity.LEFT);
    }

    @Override
    protected BasePresenter getPresenter() {
        return leftMenuPresenter;
    }

    @Override
    public void setUpAppearance(final AppearanceType type) {
        switch (type) {
            case TOOLBAR_MENU:
                unlockMenu();
                break;
            case TOOLBAR_NO_MENU:
                unlockMenu();
                break;
            case FULLSCREEN_MODAL:
                lockMenu();
                break;
            case NO_INFLUENCE:
                //do nothing
                break;
            default:
                break;
        }
    }
}
